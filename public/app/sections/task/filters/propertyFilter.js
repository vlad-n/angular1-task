app.filter(
    "propertyFilter",
    function () {
        return function (task, obj) {

            if (obj.personal == false && obj.urgent == false && obj.night == false) {
                return task;
            }
            else {
                return task.filter(function (item) {
                    var propertyArray = Object.keys(obj);
                    var check = [];
                    propertyArray.forEach(function (prop) {

                        if (obj[prop].toString() == item[prop]) {
                            check.push("true");
                        }
                        else {
                            check.push("false");
                        }
                    });

                    function checkTrue(element, index, array) {
                        return element == "true";
                    };
                    return check.every(checkTrue);
                });
            }
        };


    });