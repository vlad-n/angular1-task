app.controller('Task', ['$scope', '$rootScope', '$httpReq', function ($scope, $rootScope, $httpReq) {

    console.log("Task");

    $scope.tasks = [];
    $scope.state = "new";
    $scope.tasksCount = {
        in: 0,
        new: 0,
        underConsideration: 0,
        done: 0,
        canceled: 0,
        draft: 0
    }

    $scope.filterObj = {
        state: $scope.state
    }

    $scope.checkBoxState = {
        personal: false,
        night: false,
        urgent: false
    }

    $scope.activeFilterState = "new";

    $httpReq.postjson("/gettasks", {}).then(function (data) {
        console.log(data);
        $scope.tasks = data.tasks;

        var propertyArray = Object.keys($scope.tasksCount);

        $scope.tasks.forEach(function (task, i, arr) {

            propertyArray.forEach(function (prop, i, arr) {
                if (prop == task.state) {
                    $scope.tasksCount[prop]++
                }
                else {

                }
            });

        });
    });

}]);