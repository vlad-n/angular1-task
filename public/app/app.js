var app = angular.module('TestTaskWow', ["ui.router", "ui.bootstrap"]);
app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider.state('task', {
        name: 'task',
        url: '/task',
        controller: 'Task',
        templateUrl: 'app/sections/task/views/task.html'
    });

    $stateProvider.state('statistics', {
        name: 'statistics',
        url: '/statistics',
        controller: 'Statistics',
        templateUrl: 'app/sections/statistics/views/statistics.html'

    });

    $stateProvider.state('finance', {
        name: 'finance',
        url: '/finance',
        controller: 'Finance',
        templateUrl: 'app/sections/finance/views/finance.html'

    });

    $stateProvider.state('company', {
        name: 'company',
        url: '/company',
        controller: 'Company',
        templateUrl: 'app/sections/company/views/company.html'

    });

});