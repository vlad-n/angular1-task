app.service(
    "$httpReq",
    function ($http, $q, $rootScope, $location, $timeout, appConfig) {
        return ({
            postjson: postjson,
        });

        function postjson(reqUrl, data) {
            var deferred = $q.defer();
            $http({
                url: appConfig.serverUrl + reqUrl,
                method: "POST",
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            }).then(function success(response) {
                return deferred.resolve(response.data);
            }, function error(response) {
                return deferred.reject(response);
            }).finally(function () {

            });
            return deferred.promise;
        };


    });