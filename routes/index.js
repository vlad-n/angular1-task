var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index');
});


router.post('/gettasks', function (req, res, next) {

    var des = "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.";
    var task = [{
        id: "001",
        city: {id: "001", name: "Самара"},
        name: des,
        data: "14 декабря",
        sum: "10 000",
        state: "in",
        personal: "false",
        night: "true",
        urgent: "false"
    }, {
        id: "002",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        data: "14 декабря",
        state: "in",
        personal: "true",
        night: "false",
        urgent: "true"
    }, {
        id: "003",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        data: "14 декабря",
        state: "in",
        personal: "false",
        night: "false",
        urgent: "true"
    }, {
        id: "004",
        city: {id: "001", name: "Самара"},
        data: "14 декабря",
        name: des,
        sum: "10 000",
        state: "in",
        personal: "true",
        night: "true",
        urgent: "false"
    }, {
        id: "005",
        city: {id: "001", name: "Самара"},
        data: "14 декабря",
        name: des,
        sum: "10 000",
        personal: "true",
        state: "new",
        personal: "false",
        night: "true",
        urgent: "false"
    }, {
        id: "006",
        city: {id: "001", name: "Самара"},
        data: "14 декабря",
        name: des,
        sum: "10 000",
        state: "new",
        personal: "true",
        night: "false",
        urgent: "true"
    }, {
        id: "007",
        city: {id: "001", name: "Самара"},
        name: des,
        data: "14 декабря",
        sum: "10 000",
        data: "14 декабря",
        state: "new",
        personal: "true",
        night: "true",
        urgent: "true"
    }, {
        id: "008",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "new",
        personal: "true",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "009",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "in",
        personal: "false",
        data: "14 декабря",
        night: "true",
        urgent: "true"
    }, {
        id: "0010",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "underConsideration",
        personal: "false",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "0011",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "underConsideration",
        personal: "true",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "0012",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "underConsideration",
        personal: "false",
        data: "14 декабря",
        night: "true",
        urgent: "false"
    }, {
        id: "0013",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "underConsideration",
        data: "14 декабря",
        personal: "true",
        night: "true",
        urgent: "true"
    }, {
        id: "0014",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "done",
        personal: "true",
        data: "14 декабря",
        night: "false",
        urgent: "true"
    }, {
        id: "0015",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "done",
        personal: "true",
        data: "14 декабря",
        night: "true",
        urgent: "false"
    }, {
        id: "0016",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "done",
        personal: "false",
        data: "14 декабря",
        night: "true",
        urgent: "false"
    }, {
        id: "0017",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "done",
        personal: "false",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "0018",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "done",
        personal: "true",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "0019",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        personal: "false",
        data: "14 декабря",
        night: "false",
        urgent: "true"
    }, {
        id: "0020",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        personal: "false",
        data: "14 декабря",
        night: "false",
        urgent: "false"
    }, {
        id: "0021",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        data: "14 декабря",
        personal: "false",
        night: "true",
        urgent: "false"
    }, {
        id: "0022",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        personal: "true",
        data: "14 декабря",
        night: "false",
        urgent: "true"
    }, {
        id: "0023",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        personal: "true",
        data: "14 декабря",
        night: "true",
        urgent: "true"
    }, {
        id: "0024",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "canceled",
        personal: "true",
        data: "14 декабря",
        night: "true",
        urgent: "true"
    }, {
        id: "0025",
        city: {id: "001", name: "Самара"},
        name: des,
        sum: "10 000",
        state: "draft",
        personal: "true",
        data: "14 декабря",
        night: "true",
        urgent: "true"
    }
    ];

    res.send({tasks: task});
});

module.exports = router;
